﻿using UnityEngine;
using System.Collections;
using System;

public enum ReaperForm
{
    REAPER,
    CAT,
    BALL
}

public class Player : MonoBehaviour
{
    public float speed = 5f;
    public ReaperForm currentForm = ReaperForm.REAPER;    

    ReaperForm previousForm;

    Rigidbody2D playerRb2D;
    Animator playerAnim;
    SpriteRenderer playerRenderer;

    bool isMorphingA = false;
    bool isMorphingB = false;

    void Awake()
    {
        playerRb2D = GetComponent<Rigidbody2D>();
        playerAnim = GetComponentInChildren<Animator>();
        playerRenderer = GetComponentInChildren<SpriteRenderer>();
    }

    void FixedUpdate()
    {
        if (!GameManager.isGameOver)
        {

            // Move horizontally //                      
            if (InputManager.InputHorizontal > 0)
            {
                playerRb2D.velocity = Vector2.right * speed;
                playerRenderer.flipX = false;
            }
            else if (InputManager.InputHorizontal < 0 && !GameManager.isGameWon)
            {
                playerRb2D.velocity = Vector2.left * speed;
                playerRenderer.flipX = true;
            }

            if (InputManager.InputMorphA > 0 && !isMorphingA)
            {
                isMorphingA = true;
                if (!playerAnim.IsInTransition(0))
                {
                    // go back to REAPER from BALL
                    if (currentForm == ReaperForm.BALL)
                    {
                        currentForm = ReaperForm.REAPER;
                        playerAnim.SetTrigger("BackToReaper");
                    }
                    // go to BALL from REAPER
                    else if (currentForm == ReaperForm.REAPER)
                    {
                        currentForm = ReaperForm.BALL;
                        playerAnim.SetTrigger("ToBallMove");
                    }
                }
            }

            if (InputManager.InputMorphB > 0 && !isMorphingB)
            {
                isMorphingB = true;
                if (!playerAnim.IsInTransition(0))
                {
                    // go back to REAPER from CAT
                    if (currentForm == ReaperForm.CAT)
                    {
                        currentForm = ReaperForm.REAPER;
                        playerAnim.SetTrigger("BackToReaper");
                    }
                    // go to CAT from REAPER
                    else if (currentForm == ReaperForm.REAPER)
                    {
                        currentForm = ReaperForm.CAT;
                        playerAnim.SetTrigger("ToCatMove");
                    }
                }
            }

            if (InputManager.InputMorphA < 1 && InputManager.InputMorphB < 1)
            {
                isMorphingA = false;
                isMorphingB = false;
            }
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.name == "wintrigger")
        {
            GameManager.isGameWon = true;
        }
    }
}
