﻿using UnityEngine;
using System.Collections;

public class Mark : MonoBehaviour
{
    SpriteRenderer markSR;

    Victims victim;

    void Awake ()
    {
        markSR = GetComponent<SpriteRenderer>();
        victim = GetComponentInParent<Victims>();
    }

	// Use this for initialization
	void Start ()
    { 
        
    }
	
	// Update is called once per frame
	void Update () {
        if (victim.currentState == VictimState.ALIVE)
        {
            if (victim.currentMarked == VictimMarked.LET_LIVE)
            {
                markSR.enabled = false;
            }
            else if (victim.currentMarked == VictimMarked.LET_DIE)
            {
                markSR.enabled = true;
            }
        }
        else
        {
            markSR.enabled = false;
        }
    }
}
