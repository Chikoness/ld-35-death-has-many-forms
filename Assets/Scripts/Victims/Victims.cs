﻿using UnityEngine;
using System.Collections;

public enum VictimState
{
    ALIVE,
    DEAD
}

public enum VictimWants
{
    BALL,
    CAT
}

public enum VictimMarked
{
    LET_LIVE,
    LET_DIE
}

public class Victims : MonoBehaviour
{

    public DisplayThoughtBubble thoughtBubble;
    public VictimState currentState = VictimState.ALIVE;
    public VictimWants currentWant;
    public VictimMarked currentMarked;

    private Animator victimAnim;
    private AudioSource victimAudio;

    public bool touchedByReaper = false;

    void Awake()
    {
        thoughtBubble = GetComponentInChildren<DisplayThoughtBubble>();
        thoughtBubble.enabled = false;

        victimAnim = GetComponent<Animator>();
        victimAudio = GetComponent<AudioSource>();

        currentWant = (VictimWants)Random.Range(0, 2);
        currentMarked = (VictimMarked)Random.Range(0, 2);
    }

    // Use this for initialization
    void Start()
    {        
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            ReaperForm currentForm = collision.GetComponentInParent<Player>().currentForm;

            if (!touchedByReaper)
            {
                if (currentForm == ReaperForm.REAPER)
                {
                    if (currentMarked == VictimMarked.LET_LIVE)
                    {
                        GameManager.soulsMistakenCount++;
                        thoughtBubble.textType = GoodOrBad.Bad;
                        thoughtBubble.enabled = true;
                        thoughtBubble.hasDisplayed = false;
                    }
                    else
                    {
                        GameManager.soulsTakenCount++;
                        thoughtBubble.textType = GoodOrBad.Good;
                        thoughtBubble.enabled = true;
                        thoughtBubble.hasDisplayed = false;
                    }

                    victimAnim.SetTrigger("ToDie");
                    victimAudio.Play();

                    currentState = VictimState.DEAD;
                }
                else if (currentForm == ReaperForm.BALL)
                {
                    if (currentWant == VictimWants.CAT)
                    {
                        GameManager.soulsMistakenCount++;
                        thoughtBubble.textType = GoodOrBad.Bad;
                        thoughtBubble.enabled = true;
                        thoughtBubble.hasDisplayed = false;
                        victimAnim.SetTrigger("ToDie");
                        victimAudio.Play();
                        currentState = VictimState.DEAD;
                        GameManager.mistakenWantsCount++;
                    }
                    else
                    {
                        if (currentMarked == VictimMarked.LET_DIE)
                        {
                            GameManager.soulsMissedCount++;
                        }
                    }
                }
                else if (currentForm == ReaperForm.CAT)
                {
                    if (currentWant == VictimWants.BALL)
                    {
                        GameManager.soulsMistakenCount++;
                        thoughtBubble.textType = GoodOrBad.Bad;
                        thoughtBubble.enabled = true;
                        thoughtBubble.hasDisplayed = false;
                        victimAnim.SetTrigger("ToDie");
                        victimAudio.Play();
                        currentState = VictimState.DEAD;
                        GameManager.mistakenWantsCount++;
                    }
                    else
                    {
                        if (currentMarked == VictimMarked.LET_DIE)
                        {
                            GameManager.soulsMissedCount++;
                        }
                    }
                }

                touchedByReaper = true;
            }
        }
    }
}
