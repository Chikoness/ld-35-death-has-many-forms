﻿using UnityEngine;
using System.Collections;

public class CurrentWantIndicator : MonoBehaviour {

    public Sprite[] wantSprites = new Sprite[2];

    private SpriteRenderer cwIndicatorSR;
    private Animator cwIndicatorAnim;
    private Victims victims;

    void Awake ()
    {
        cwIndicatorSR = GetComponent<SpriteRenderer>();
        cwIndicatorAnim = GetComponent<Animator>();
        victims = GetComponentInParent<Victims>();
    }

	// Use this for initialization
	void Start ()
    {
        cwIndicatorAnim.speed = Random.Range(0, 1.0f);
	}
	
	// Update is called once per frame
	void Update () {
        if (!victims.touchedByReaper)
        {
            if (victims.currentWant == VictimWants.BALL)
            {
                cwIndicatorSR.sprite = wantSprites[(int)VictimWants.BALL];
            }
            else if (victims.currentWant == VictimWants.CAT)
            {
                cwIndicatorSR.sprite = wantSprites[(int)VictimWants.CAT];
            }
        }
        else
        {
            cwIndicatorSR.enabled = false;
            cwIndicatorAnim.enabled = false;
        }
    }
}
