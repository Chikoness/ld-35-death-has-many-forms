﻿using UnityEngine;
using System.Collections;

public enum GoodOrBad
{
    Good,
    Bad
}

public class DisplayThoughtBubble : MonoBehaviour {

    public GoodOrBad textType;

    public Sprite[] goodThoughtBubbles = new Sprite[10];
    public Sprite[] badThoughtBubbles = new Sprite[10];

    SpriteRenderer thoughtBubbleSR;

    public bool hasDisplayed = true;

    void Awake ()
    {
        
        
    }

	// Use this for initialization
	void Start () {
        thoughtBubbleSR = GetComponent<SpriteRenderer>();

    }

    // Update is called once per frame
    void LateUpdate () {        
        if (!hasDisplayed)
        {
            if (textType == GoodOrBad.Good)
            {
                for (int i = 0; i < goodThoughtBubbles.Length; i++)
                {
                    if (i == Random.Range(0, goodThoughtBubbles.Length))
                    {
                        if (thoughtBubbleSR.sprite == null)
                        {
                            thoughtBubbleSR.sprite = goodThoughtBubbles[i];
                        }
                    }
                }
            }
            else if (textType == GoodOrBad.Bad)
            {
                for (int i = 0; i < badThoughtBubbles.Length; i++)
                {
                    if (i == Random.Range(0, badThoughtBubbles.Length))
                    {
                        if (thoughtBubbleSR.sprite == null)
                        {
                            thoughtBubbleSR.sprite = badThoughtBubbles[i];
                        }
                    }                  
                }
            }

            hasDisplayed = true;
        }
    }
}
