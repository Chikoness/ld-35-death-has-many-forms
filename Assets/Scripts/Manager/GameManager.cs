﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
        
    private static Vector2 reaperSpawnPos;
    private static Vector2 reaperCurrentPos;
    private static Vector2 reaperPreviousPos;

    public static int soulsTakenCount = 0;
    public static int soulsMistakenCount = 0;
    public static int soulsMissedCount = 0;
    public static int mistakenWantsCount = 0;

    public static bool isGameOver = false;
    public static bool isGameWon = false;
    
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        if (soulsMistakenCount > 5)
        {
            isGameOver = true;
        }

        if (isGameOver || isGameWon)
        {
            if (Input.GetKey(KeyCode.Return))
            {
                isGameOver = false;
                isGameWon = false;
                soulsTakenCount = 0;
                soulsMistakenCount = 0;
                soulsMissedCount = 0;
                mistakenWantsCount = 0;
                Scene loadedLevel = SceneManager.GetActiveScene();
                SceneManager.LoadScene(loadedLevel.buildIndex);
            }
        }
    }
}
