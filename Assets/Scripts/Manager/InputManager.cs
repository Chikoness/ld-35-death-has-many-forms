﻿using UnityEngine;
using System.Collections;

public class InputManager : MonoBehaviour {

    private static float inputHorizontal;
    private static float inputMorphA;
    private static float inputMorphB;

    public static float InputHorizontal
    {
        get
        {            
            inputHorizontal = Input.GetAxis("Horizontal");
            return inputHorizontal;
        }
    }

    public static float InputMorphA
    {
        get
        {
            inputMorphA = Input.GetKey(KeyCode.LeftShift) ? 1 : 0;
            return inputMorphA;
        }
    }

    public static float InputMorphB
    {
        get
        {
            inputMorphB = Input.GetKey(KeyCode.RightShift) ? 1 : 0;
            return inputMorphB;
        }
    }
    
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
