﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoresText : MonoBehaviour {

    Text scoresText;

    void Awake ()
    {
        scoresText = GetComponent<Text>();
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
        scoresText.text = "Souls Taken: " + GameManager.soulsTakenCount + "       "
                          + "Souls Mistaken: " + GameManager.soulsMistakenCount + "       "
                          + "Missed Souls: " + GameManager.soulsMissedCount + "       "
                          + "Souls That HATE YOU: " + GameManager.mistakenWantsCount;

    }
}
