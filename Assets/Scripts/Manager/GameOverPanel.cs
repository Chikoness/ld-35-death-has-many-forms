﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameOverPanel : MonoBehaviour {

    Image iamge;
    Image gameOverImage;

    void Awake ()
    {
        iamge = GetComponent<Image>();
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    if (GameManager.isGameOver)
        {
            iamge.enabled = true;
        }
        else if (!GameManager.isGameOver)
        {
            iamge.enabled = false;
        }
	}
}
